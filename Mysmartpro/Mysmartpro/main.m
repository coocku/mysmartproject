//
//  main.m
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/6/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
