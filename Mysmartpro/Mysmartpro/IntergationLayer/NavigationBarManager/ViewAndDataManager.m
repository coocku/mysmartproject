//
//  NavigationBarManager.m
//  PoprxNewDesign
//
//  Created by Swati Durugkar on 1/6/16.
//  Copyright © 2016 Swati Durugkar. All rights reserved.
//

#import "ViewAndDataManager.h"

@implementation ViewAndDataManager
+ (ViewAndDataManager *)sharedSingleton
{
    static ViewAndDataManager * _sharedMySingleton;
    @synchronized(self)
    {
        if (!_sharedMySingleton)
            _sharedMySingleton = [[ViewAndDataManager alloc] init];
        
        return _sharedMySingleton;
    }
}


-(void)makeNavigationBarWithMenuAndRightButton:(UINavigationController *)classNavigationController targetOfButton:(id)target rightButtonAction:(SEL)rightSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem
{
    
    [classNavigationController.navigationBar setTranslucent:NO];
    
    // Add custom lable to show title on navigation bar
     UILabel * lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 110, 44)];
    [lblTitle setFont:[UIFont systemFontOfSize:16.0]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setText:titleString];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [navItem setTitleView:lblTitle];
    
}


-(void)makeNavigationBarWithBackButton:(UINavigationController *)classNavigationController targetOfButton:(id)target backButtonAction:(SEL)backSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem{

    
    // Add custom lable to show title on navigation bar
    UILabel * lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 110, 44)];
    [lblTitle setFont:[UIFont systemFontOfSize:16.0]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setText:titleString];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [navItem setTitleView:lblTitle];

    UIButton * btn_back=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_back setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateHighlighted];
    [btn_back setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateNormal];
    [btn_back setFrame:CGRectMake(270, 3, 35, 35)];
    [btn_back addTarget:target action:backSelector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton=[[UIBarButtonItem alloc]initWithCustomView:btn_back];
    [navItem setLeftBarButtonItem:leftBarButton];
}

-(void)makeNavigationBarWithTitleLable:(UINavigationController *)classNavigationController targetOfButton:(id)target backButtonAction:(SEL)backSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem{
    
    // Add custom lable to show title on navigation bar
    UILabel * lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 44)];
    [lblTitle setFont:[UIFont systemFontOfSize:16.0]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setText:titleString];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [navItem setTitleView:lblTitle];

}



-(void)makeNavigationBarWithBackAndRightButton:(UINavigationController *)classNavigationController targetOfButton:(id)target rightButtonAction:(SEL)rightSelector backButtonAction:(SEL)backSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem
{
    
    // Add custom lable to show title on navigation bar
    UILabel * lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 110, 44)];
    [lblTitle setFont:[UIFont systemFontOfSize:16.0]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setText:titleString];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [navItem setTitleView:lblTitle];
    
    UIButton * btn_back=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_back setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateHighlighted];
    [btn_back setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateNormal];
    [btn_back setFrame:CGRectMake(270, 3, 35, 35)];
    [btn_back addTarget:target action:backSelector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton=[[UIBarButtonItem alloc]initWithCustomView:btn_back];
    [navItem setLeftBarButtonItem:leftBarButton];


    UIButton * btn_AddToCart=[UIButton buttonWithType:UIButtonTypeCustom];
//    [btn_AddToCart setBackgroundImage:[UIImage imageNamed:kIconCheckMark] forState:UIControlStateHighlighted];
//    [btn_AddToCart setBackgroundImage:[UIImage imageNamed:kIconCheckMark] forState:UIControlStateNormal];
    [btn_AddToCart setTitle:@"" forState:UIControlStateNormal];
    [btn_AddToCart setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btn_AddToCart setFrame:CGRectMake(270, 3, 24, 24)];
    [btn_AddToCart addTarget:target action:rightSelector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton2=[[UIBarButtonItem alloc]initWithCustomView:btn_AddToCart];
    [navItem setRightBarButtonItem:rightBarButton2];
}



/**
 *  Make navigation bar with custom menu or back left button and right button
 *
 *  @param classNavigationController navigation controller object
 *  @param target                    target of buttons
 *  @param rightSelector             right button action
 *  @param rightBtnTitle             right button title
 *  @param imageName                 right button image name
 *  @param titleString               navigation bar title
 *  @param navItem                   navigation items object
 *  @param backSelector              back button selector
 *  @param isMenu                    Yes if left button is menu
 */
-(void)makeNavigationBarWithLeftAndRightCustomButton:(UINavigationController *)classNavigationController targetOfButton:(id)target rightButtonAction:(SEL)rightSelector andRightBtnTitle:(NSString *)rightBtnTitle rightBtnImage:(NSString *)imageName andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem backButtonAction:(SEL)backSelector isMenuButton:(BOOL)isMenu
{
    
    [classNavigationController setNavigationBarHidden:NO];
    [classNavigationController.navigationBar setTranslucent:NO];
    
    // Add custom lable to show title on navigation bar
    UILabel * lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 110, 44)];
    [lblTitle setFont:[UIFont systemFontOfSize:16.0]];
    [lblTitle setText:titleString];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [navItem setTitleView:lblTitle];
    
    if (isMenu) {
        //added by swati change left menu icon
//        [target addLeftMenuButtonWithImage:[UIImage imageNamed:kMenuIcon]];

    }
    else{
        UIButton * btn_back=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn_back setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateHighlighted];
        [btn_back setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateNormal];
        btn_back.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
        [btn_back setFrame:CGRectMake(20, 5, 35, 35)];
        [btn_back addTarget:target action:backSelector forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton=[[UIBarButtonItem alloc]initWithCustomView:btn_back];
        [navItem setLeftBarButtonItem:leftBarButton];
    }
    
    UIButton * btn_AddToCart=[UIButton buttonWithType:UIButtonTypeCustom];
    if (imageName!=nil && rightBtnTitle.length==0) {
        [btn_AddToCart setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }else if (imageName!=nil)
    {
        [btn_AddToCart setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [btn_AddToCart setTitle:rightBtnTitle forState:UIControlStateNormal];
    }
    else{
        [btn_AddToCart setTitle:rightBtnTitle forState:UIControlStateNormal];

    }
    [btn_AddToCart setFrame:CGRectMake(20, 3, 77, 21)];
    btn_AddToCart.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btn_AddToCart addTarget:target action:rightSelector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton=[[UIBarButtonItem alloc]initWithCustomView:btn_AddToCart];
    [navItem setRightBarButtonItem:rightBarButton];
    
}


-(void)showAlertView:(NSString *)setMessage inView:(UIView *)viewC
{
    //[YRDropdownView  showDropdownInView:viewC title:nil detail:setMessage BackgroundColor:kBackgroundColorOfOrangeButton];
    [self performSelector:@selector(hideAlertView) withObject:nil afterDelay:1.5];
}
-(void)hideAlertView
{
//    [YRDropdownView hideDropdownInView:[UIApplication sharedApplication].keyWindow ];
}


-(NSString *)setValueInDictionaryIfNil:(NSString *)strValue
{
    NSString * str;
    if (![strValue isKindOfClass:[NSString class]]) {
        strValue = [NSString stringWithFormat:@"%@",strValue];
    }
    
    if ([strValue isKindOfClass:[NSNull class]]) {
         str = @"";
    }
    else if (strValue.length==0  ||strValue == nil) {
        str = @"";
    }else if (strValue.length!=0)
    {
        str = strValue;
    }
    return str;
}

@end
