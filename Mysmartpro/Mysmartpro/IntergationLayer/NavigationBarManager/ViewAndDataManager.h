//
//  NavigationBarManager.h
//  PoprxNewDesign
//
//  Created by Swati Durugkar on 1/6/16.
//  Copyright © 2016 Swati Durugkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ViewAndDataManager : NSObject

+(ViewAndDataManager *)sharedSingleton;


-(void)makeNavigationBarWithMenuAndRightButton:(UINavigationController *)classNavigationController targetOfButton:(id)target rightButtonAction:(SEL)rightSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem;


-(void)makeNavigationBarWithBackAndRightButton:(UINavigationController *)classNavigationController targetOfButton:(id)target rightButtonAction:(SEL)rightSelector backButtonAction:(SEL)backSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem;



-(void)makeNavigationBarWithBackButton:(UINavigationController *)classNavigationController targetOfButton:(id)target backButtonAction:(SEL)backSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem;


-(void)makeNavigationBarWithTitleLable:(UINavigationController *)classNavigationController targetOfButton:(id)target backButtonAction:(SEL)backSelector andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem;


-(void)makeNavigationBarWithLeftAndRightCustomButton:(UINavigationController *)classNavigationController targetOfButton:(id)target rightButtonAction:(SEL)rightSelector andRightBtnTitle:(NSString *)rightBtnTitle rightBtnImage:(NSString *)imageName andTitle:(NSString *)titleString andNavigationItem:(UINavigationItem *)navItem backButtonAction:(SEL)backSelector isMenuButton:(BOOL)isMenu;


-(void)showAlertView:(NSString *)setMessage inView:(UIView *)viewC;


-(void)hideAlertView;

-(NSString *)setValueInDictionaryIfNil:(NSString *)strValue;
@end
