//
//  ProjectSelectionCell.m
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/8/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import "ProjectSelectionCell.h"

@implementation ProjectSelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
