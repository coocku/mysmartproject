//
//  ProjectImagesCell.m
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/8/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import "ProjectImagesCell.h"

@implementation ProjectImagesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.ImageCollection setPagingEnabled:YES];
    [self.ImageCollection setCollectionViewLayout:flowLayout];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
