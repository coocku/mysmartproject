//
//  ProjectImagesCell.h
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/8/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectImagesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *ImageCollection;

@end
