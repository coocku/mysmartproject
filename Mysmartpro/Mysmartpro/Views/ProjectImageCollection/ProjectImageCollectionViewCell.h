//
//  ProjectImageCollectionViewCell.h
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/8/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCollection;

@end
