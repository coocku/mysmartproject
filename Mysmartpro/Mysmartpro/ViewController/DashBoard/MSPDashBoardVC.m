//
//  MSPDashBoardVC.m
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/8/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import "MSPDashBoardVC.h"
#import "ProjectSelectionCell.h"
#import "ProjectImagesCell.h"
#import "ViewAndDataManager.h"
#import "ProjectImageCollectionViewCell.h"
#import "ProjectDetailCellTableViewCell.h"

#define kProjectSelectionCell @"ProjectSelectionCell"
#define kProjectImagesCell @"ProjectImagesCell"
#define kProjectDetailCellTableViewCell @"ProjectDetailCellTableViewCell"


@interface MSPDashBoardVC ()

@property (weak, nonatomic) IBOutlet UITableView *tblDashBoard;

@end

@implementation MSPDashBoardVC

#pragma -
#pragma - View life cycle

- (void)viewDidLoad {
    [self PrepareLayout];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma -
#pragma - Prepare layout

-(void)PrepareLayout{
    
    
    [self.tblDashBoard registerNib:[UINib nibWithNibName:@"ProjectSelectionCell" bundle:nil] forCellReuseIdentifier:kProjectSelectionCell];
    [self.tblDashBoard registerNib:[UINib nibWithNibName:@"ProjectImagesCell" bundle:nil] forCellReuseIdentifier:kProjectImagesCell];
    [self.tblDashBoard registerNib:[UINib nibWithNibName:@"ProjectDetailCellTableViewCell" bundle:nil] forCellReuseIdentifier:kProjectDetailCellTableViewCell];

    
    [[ViewAndDataManager sharedSingleton]makeNavigationBarWithBackAndRightButton:self.navigationController targetOfButton:self rightButtonAction:@selector(backSelector:) backButtonAction:@selector(backSelector:) andTitle:@"My SmartPro" andNavigationItem:self.navigationItem];

}


#pragma -
#pragma - All buttons IBAction

-(IBAction)backSelector:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark -  TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    switch (indexPath.section) {
        case 0:{
            return 80;
            break;
        }
        case 1:{
            return 115;
            break;
        }
        case 2:{
            return 478;
            break;
        }
        case 3:{
            return 80;
            break;
        }
        case 4:{
            return 80;
            break;
        }
        case 5:{
            return 80;
            break;
        }
        case 6:{
            return 80;
            break;
        }
        default:{
            return 80;
            break;
        }
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            ProjectSelectionCell *dashboardCell=[tableView dequeueReusableCellWithIdentifier:kProjectSelectionCell forIndexPath:indexPath];
            return dashboardCell;
            break;
        }
        case 1:{
            
            ProjectImagesCell *imageCell=[tableView dequeueReusableCellWithIdentifier:kProjectImagesCell forIndexPath:indexPath];
            
            UINib *cellNib = [UINib nibWithNibName:@"ProjectImageCollectionViewCell" bundle:nil];
            [imageCell.ImageCollection registerNib:cellNib forCellWithReuseIdentifier:@"ProjectImageCollectionViewCell"];
            
            return imageCell;
        }
        default:{
            ProjectDetailCellTableViewCell *detailCell=[tableView dequeueReusableCellWithIdentifier:kProjectDetailCellTableViewCell forIndexPath:indexPath];
            return detailCell;
            break;
            break;
        }
    }
}




#pragma mark UICollectionView Delegates/DataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProjectImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProjectImageCollectionViewCell" forIndexPath:indexPath];
    
    if (indexPath.item==0) {
        [cell.imgCollection setBackgroundColor:[UIColor greenColor]];
    }else if (indexPath.item==0) {
        [cell.imgCollection setBackgroundColor:[UIColor blueColor]];
    }else{
    [cell.imgCollection setBackgroundColor:[UIColor greenColor]];
    }

    return cell;
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
