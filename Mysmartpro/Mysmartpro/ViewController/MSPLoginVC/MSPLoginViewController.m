//
//  MSPLoginViewController.m
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/7/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import "MSPLoginViewController.h"
#import "MSPDashBoardVC.h"
@interface MSPLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtXompanyId;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginAction:(id)sender;


@end

@implementation MSPLoginViewController

#pragma -
#pragma - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma -
#pragma - Prepare layout

-(void)PrepareLayout{

    
}

#pragma -
#pragma - All buttons IBAction

- (IBAction)btnLoginAction:(id)sender {
    
//    AppDelegate *appd = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    MSPDashBoardVC *objMSPDashBoardVC = [[AppDelegate getStoryboard] instantiateViewControllerWithIdentifier:@"MSPDashBoardVC"];
    [self.navigationController pushViewController:objMSPDashBoardVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
