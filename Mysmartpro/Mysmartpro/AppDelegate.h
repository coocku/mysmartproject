//
//  AppDelegate.h
//  Mysmartpro
//
//  Created by Chetan Sharma on 2/6/17.
//  Copyright © 2017 Chetan Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+(UIStoryboard *)getStoryboard;
@end

